import React,{ useState, createContext,useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack'
import { LoginScreen } from './screens/LoginScreen';
import { ListMoviesScreen } from './screens/ListMoviesScreen';
import { MovieDetailsScreen } from './screens/MovieDetailsScreen';
import {Alert,Text, TouchableOpacity} from 'react-native'

const Stack = createStackNavigator()
const AuthContext = createContext()
export const context = ()=> useContext(AuthContext);

export default function App() {
  
  const [login, setlogin] = useState({logged:false,crendentials:{ user:'admin',password:'123'}})
  const {logged} = login
  const handleLogin = () => setlogin({...login,logged:true})  
  const handleLogout = () => {
    Alert.alert(
      "Logout",
      "¿Estas seguro que quieres salir?",
      [
        {
          text:'Cancel',
          onPress: ()=> console.log("cancelado"),
          style:'cancel'
        },
        {
          text:'Yes',
          onPress:()=>setlogin({...login,logged:false}),

        },
      ],
      {cancelable:false}
    )
  }
  return (
    <AuthContext.Provider value={{login,handleLogin}}>
        <NavigationContainer>
          <Stack.Navigator screenOptions={{headerStyle:{backgroundColor:'#1A202A'},headerTintColor:'#FFF'}}>
              {
                logged?(
                <>
                    <Stack.Screen name="ListMovies" component={ListMoviesScreen} options={{headerRight:()=>(
                    <TouchableOpacity onPress={()=> handleLogout()} style={{marginRight:20}}>
                      <Text style={{color:'#079DCF'}}>Salir</Text>
                    </TouchableOpacity>)}}/>

                    <Stack.Screen name="MovieDetails" component={MovieDetailsScreen}/>
                </>
                ):
                (
                  <>
                    <Stack.Screen name="Login" component={LoginScreen} options={{headerShown:false}}/>
                  </>
                )
              }
          </Stack.Navigator>
        </NavigationContainer>
    </AuthContext.Provider>
  );
}


