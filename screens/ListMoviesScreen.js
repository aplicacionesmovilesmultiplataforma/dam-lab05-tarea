import React, { useState, useEffect } from 'react'
import { Text, View, Image, StyleSheet, FlatList, TouchableOpacity} from 'react-native'


export const ListMoviesScreen = ({navigation}) => {

    const [data, setdata] = useState([])
    useEffect(() => {
        
        fetch('https://yts.mx/api/v2/list_movies.json')
        .then( res => res.json())
        .then(resp => setdata(resp.data.movies) )
    }, [])
    const handleItem = (id) => {
        navigation.navigate('MovieDetails',{ idItem:id})
    }
    const Item = ({id, name, descripcion, imagen}) => {
    
        return(
            <TouchableOpacity style={styles.item} onPress={ ()=> handleItem(id)}>
                <View style={styles.contianerImage}>
                    <Image source={{uri:imagen}}
                        style={styles.image}
                    />
                </View>
                <View>
                    <View style={styles.containerTitle}>
                        <Text style={styles.title}>
                            { name }
                        </Text>
                    </View>
                    <View style={styles.containerDescripcion}>
                        <Text style={styles.descripcion}>
                            { descripcion }
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    return (
        <View>
            <FlatList  
                data={ data }
                renderItem={
                    ({item}) => (
                        <Item
                            id={ item.id }
                            name={ item.title }
                            descripcion={ item.description_full }
                            imagen={ item.small_cover_image }
                        />
                    )
                }
                keyExtractor={ item => item.id.toString() }
                />

        </View>
    )
}
const styles = StyleSheet.create({
    item:{
        borderBottomWidth:0.6,
        borderBottomColor:'#595959',
        padding:15,
        marginVertical:3,
        marginHorizontal:7,
        flex:1,
        flexDirection:'row',
    },
    contianerImage:{
        borderColor: 'black',
        marginRight:20
    },
    image:{
        height:70,
        width:70,
        borderRadius:35
    },
    
    containerTitle:{
        width:230,
        height:25,
        overflow:'hidden',
    },
    title:{
        fontSize:17,
        fontWeight:'bold',
        color:'#464545'
    },
    descripcion:{

    },
    containerDescripcion:{
        width:240,
        height:40,
        overflow:'hidden'
    }
})
