import React, {useEffect, useState} from 'react'
import { Text, View, StyleSheet,Image } from 'react-native'

export const MovieDetailsScreen = ({route}) => {
    
    const [item, setItem] = useState({})
    useEffect(() => {
        fetch(`https://yts.mx/api/v2/movie_details.json?movie_id=${route.params.idItem}`)
        .then(res => res.json())
        .then(res => setItem(res.data.movie))
    }, [])
    
    return (
        <View>
            <View>
                <Image source={{uri: item.background_image_original }} style={styles.image}/>
            </View>
            <View style={styles.containerContent}>
                <View>
                    <Text style={styles.name}>{ item.title }</Text>
                </View>
                <View>
                    <Text>{ item.description_intro }</Text>
                </View>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    containerImage:{

    },
    image:{
        width:400,
        height:400
    },
    containerContent:{
        margin: 20
    },
    containerName:{
        margin:10
    },
    name:{
        fontSize:20
    },
    containerDescription:{

    }
})
