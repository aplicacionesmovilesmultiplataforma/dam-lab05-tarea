import React,{ useState } from 'react'
import {
    StyleSheet,
    Text,
    TextInput,
    View, Image,
    TouchableOpacity
} from 'react-native';
import { context } from '../App';

export const LoginScreen = (props) => {
    const {handleLogin,login:{crendentials}} = context()
    
    const [state, setState] = useState({
        user:'',
        pass:''
    });
    
    const handleChangeInput = (item,value) => {
        setState({...state,[item]:value})
    }
    const handleOnPress = () => {
        if(state.user === crendentials.user && state.pass === crendentials.password){
            alert("Ingresando...")
            handleLogin()
        }else{
            alert("Las credenciales ingresadas son incorrectas")
        }
        
    }
    
    return(
        <View style={styles.loginContainer}>
            <View style={styles.container}>
                <Text style={styles.textSize}>Login</Text>
                
                <View style={styles.containerInput}>
                    <TextInput style={styles.inputStylesLogin}
                        onChangeText={ value => handleChangeInput('user',value)}
                        value={ state.user}
                    />
                </View>
                <View style={styles.containerInput}>
                    <TextInput secureTextEntry={true} style={styles.inputStylesLogin}
                        onChangeText={ value => handleChangeInput('pass',value)}
                        value={ state.pass}
                    />
                </View>
            </View>
            <View style={styles.containerBtn}>
                <TouchableOpacity style={styles.btnLogin} onPress={handleOnPress}>
                        <Text style={styles.textSizeBtn}>
                            Ingresar
                        </Text>
                </TouchableOpacity>
            </View>
        </View>
        )
    
}

const styles = StyleSheet.create({
        loginContainer:{
        width: 'auto',
        flex:1,
        backgroundColor:'#11161C'
        },
        container:{
        marginTop:120,
        },
        textSize:{
        marginBottom:40,
        fontSize:30,
        textAlign:'center',
        color:'#fff'
        },
        inputStylesLogin:{
        height:50,
        width:300,
        backgroundColor:'#1A202A',
        color:'#fff',
        fontSize:17,
        borderRadius:10,
        borderColor:'#fff',
        },
        containerInput:{
        margin:10,
        alignItems:'center'
        },
        containerBtn:{
        marginTop:30,
        alignItems:'center'
        },
        textSizeBtn:{
        fontSize:20
        }
        ,
        btnLogin:{
        width:200,
        height:60,
        backgroundColor:'#4DD8C1',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:10,
        },
        img:{
        width:300,
        height:300
        },
        containerImg:{
        alignItems:'center'
        }
    });
    